﻿using System;
		
using UIKit;
using RabbitMQ.Client;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client.Events;

namespace bunny.iOS
{
	public partial class ViewController : UIViewController
	{
		int count = 1;

		public ViewController (IntPtr handle) : base (handle)
		{		
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			Button.AccessibilityIdentifier = "myButton";
			Button.TouchUpInside += delegate {
				var title = string.Format ("{0} clicks!", count++);
				Button.SetTitle (title, UIControlState.Normal);
				var factory = new ConnectionFactory() { HostName = "10.0.0.4", UserName="guest", Password="guest" };
				using (var connection = factory.CreateConnection())
				{
					using (var channel = connection.CreateModel())
					{
						channel.QueueDeclare(queue: "hello",
							durable: false,
							exclusive: false,
							autoDelete: false,
							arguments: null);

						var body = Encoding.UTF8.GetBytes(count.ToString());

						channel.BasicPublish(exchange: "",
							routingKey: "hello",
							basicProperties: null,
							body: body);
					}
				}
			};

			var factory1 = new ConnectionFactory () { HostName = "10.0.0.4" };
			var connection1 = factory1.CreateConnection ();
			var channel1 = connection1.CreateModel ();
			channel1.QueueDeclare (queue: "hello",
				durable: false,
				exclusive: false,
				autoDelete: false,
				arguments: null);

			var consumer = new EventingBasicConsumer (channel1);
			consumer.Received += (model, ea) => {
				var body = ea.Body;
				var message = Encoding.UTF8.GetString (body);
				BeginInvokeOnMainThread(() => lblOutput.Text = message);
			};
			channel1.BasicConsume (queue: "hello",
				noAck: false,
				consumer: consumer);
		}

		public override void DidReceiveMemoryWarning ()
		{		
			base.DidReceiveMemoryWarning ();		
			// Release any cached data, images, etc that aren't in use.		
		}
	}
}
